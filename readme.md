**Shopworks Dev Test**
===================

**Requirements: **

- php 5.6 >

- laravel 5.3


After installation, please open link @ http://localhost:8000/staff/rota

* Ensure that the given dump file is imported into your db;

* Change db settings in .env file;

* Then open the link in your prefered environment; <br />