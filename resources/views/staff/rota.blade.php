@extends('layouts.master')
@section('title', 'Staff Rota Slot by Ben Igbokwe')

@section('content')
    <div class="jumbotron sw-jumbotron">
        <h1>Daily Staff Rota</h1>
    </div>
    @if(count($rotaSlots) > 0)
        @foreach ($rotaSlots as $key => $rotaSlotBatch)
            @if(count($rotaSlotBatch) > 0)
                <h4>{{date('l', strtotime("Sunday + $key Days"))}}</h4>
                <table class="table table-bordered table table-hover">
                    <tr class="lead success">
                        <td>Staff ID</td>
                        <td>Start-time</td>
                        <td>End-time</td>
                        <td>Workhours</td>
                    </tr>
                    @php
                        $calcHoursPerDay = 0;
                    @endphp
                    @foreach($rotaSlotBatch as $batch)
                        <tr>
                           @php
                               $calcHoursPerDay += $batch->workhours;
                            @endphp
                            <td>{{$batch->staffid}}</td>
                            <td>{{$batch->starttime}}</td>
                            <td>{{$batch->endtime}}</td>
                            <td>{{$batch->workhours}}</td>
                        </tr>
                    @endforeach
                    <tr class="info">
                        <td><strong>Total number of hours worked</strong></td>
                        <td colspan="3" class="text-right"><strong>{{$calcHoursPerDay}}</strong></td>
                    </tr>
                </table><hr>
            @endif
        @endforeach
        <div class="pull-right">
            {{$rotaSlots->setPath('rota')->render()}}
        </div>
    @else
        <div class="sw-no-result-found bg-danger">
            No result(s)found!
        </div>
    @endif
@endsection
