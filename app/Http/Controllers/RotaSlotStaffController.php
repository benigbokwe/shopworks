<?php
/**
 * Created by PhpStorm.
 * User: Ben Igbokwe
 * Date: 12/11/2016
 * Time: 16:25
 */

namespace App\Http\Controllers;

use App\Model\RotaSlotStaff;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Support\Collection;

class RotaSlotStaffController extends BaseController
{
    const  MAX_ITEMS = 3;

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function listAction()
    {
        $rotaSlots = RotaSlotStaff::groupRotaByDayNumber();

        return view('staff.rota', [
            'rotaSlots' => $this->paginator($rotaSlots, 3)
        ]);
    }

    public function paginator($searchResult, $perPage)
    {
        //Get current page from url e.g. &page=2
        $currentPage = LengthAwarePaginator::resolveCurrentPage();

        //Create a new Laravel collection from the array data
        $collection = new Collection($searchResult);

        //Slice the collection to get the items to display in current page
        $currentPageSearchResults = $collection->slice(($currentPage - 1) * $perPage, $perPage)->all();

        //Create paginator and pass it to the view
        return new LengthAwarePaginator($currentPageSearchResults, count($collection), $perPage);
    }

}