<?php

/**
 * Created by PhpStorm.
 * User: Ben Igbokwe
 * Date: 12/11/2016
 * Time: 16:58
 */
namespace App\Model;

use Illuminate\Database\Eloquent\Model as BaseModel;

class RotaSlotStaff extends BaseModel
{
    /**
     * The table associated with RotaSlotStaff model.
     *
     * @var string
     */
    protected $table = 'rota_slot_staff';

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;

    const SLOT_TYPE = 'shift';

    /**
     * @return \Illuminate\Database\Eloquent\Collection|null
     */
    public static function findAllRotaSlot()
    {
        /**
         * @var $slots \Illuminate\Database\Eloquent\Collection
         */

        $slots =  RotaSlotStaff::where('slottype', '=', self::SLOT_TYPE)
            ->whereNotNull('staffid')->get();

        if($slots->isEmpty()) {
            return null;
        }

        return $slots;
    }

    /**
     * @return array|null
     */
    public static function groupRotaByDayNumber()
    {
        $slots = self::findAllRotaSlot();

        if(is_null($slots)) {
            return null;
        }

        $group = [];

        // group slots by day number
        foreach ($slots as $slot) {
            $group[$slot->daynumber][] = $slot;
        }

        // sort result set by the key - day number
        ksort($group);

        return $group;
    }

}