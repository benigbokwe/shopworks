<?php
/**
 * Created by PhpStorm.
 * User: Ben Igbokwe
 * Date: 12/11/2016
 * Time: 23:31
 */

class RotaSlotStaffControllerTest extends TestCase
{
    public function testListAction()
    {
        $this->visit('/staff/rota')
            ->see('Daily Staff Rota');
    }
}