<?php
/**
 * Created by PhpStorm.
 * User: Ben Igbokwe
 * Date: 12/11/2016
 * Time: 23:57
 */

class RotaSlotStaffTest extends \TestCase
{
    public function testFindAllRotaSlot()
    {
        //call
        $slots = \App\Model\RotaSlotStaff::findAllRotaSlot();

        $this->assertInstanceOf(\App\Model\RotaSlotStaff::class, $slots[0]);
        $this->assertInstanceOf(\Illuminate\Database\Eloquent\Collection::class, $slots);
    }
}